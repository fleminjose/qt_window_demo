#include "display_details.h"
#include "ui_display_details.h"
#include "access_page.h"
#include <QDesktopWidget>

display_details::display_details(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::display_details)
{
    ui->setupUi(this);
}

void display_details::windowShow(access_page *accessClass){
    access_obj = accessClass;
    move(QApplication::desktop()->screen()->rect().center() - rect().center());
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    show();
    qDebug("name: " + name.toLatin1());
    this->ui->name->setText(name);
    this->ui->email->setText(email);
    this->ui->phone->setText(phone);
}

void display_details::fillAccessDetails(access_page *accessClass){
    name = accessClass->name;
    email = accessClass->email;
    phone = accessClass->phone;
}


display_details::~display_details()
{
    delete ui;
}

void display_details::on_pushButton_2_clicked()
{
    hide();
    access_obj->move(QApplication::desktop()->screen()->rect().center() - access_obj->rect().center());
    access_obj->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    access_obj->show();
}

void display_details::on_pushButton_clicked()
{
    exit(0);
}
