#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    if(ui->lineEdit->text().toStdString() == "1234")
    {
        this->hide();
        qDebug("\npassword is correct..   :)");
        access.move(QApplication::desktop()->screen()->rect().center() - access.rect().center());
        access.setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
        access.show();
    }
    else{
        qDebug("\npassword is wrong..   :(\n");
    }
}
