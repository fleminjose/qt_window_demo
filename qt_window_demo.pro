#-------------------------------------------------
#
# Project created by QtCreator 2019-06-11T21:14:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt_window_demo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    access_page.cpp \
    display_details.cpp

HEADERS  += mainwindow.h \
    access_page.h \
    display_details.h

FORMS    += mainwindow.ui \
    access_page.ui \
    display_details.ui
