#include "access_page.h"
#include "ui_access_page.h"
#include <QDesktopWidget>

access_page::access_page(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::access_page)
{
    ui->setupUi(this);
}

access_page::~access_page()
{
    delete ui;
}

void access_page::on_pushButton_clicked()
{
    this->hide();
    this->name = QString::fromStdString(this->ui->lineEdit->text().toStdString());
    this->email = QString::fromStdString(this->ui->lineEdit_2->text().toStdString());
    this->phone = QString::fromStdString(this->ui->lineEdit_3->text().toStdString());
    qDebug("name: " + name.toLatin1());
    disp_details.fillAccessDetails(this);
    disp_details.windowShow(this);
}

void access_page::on_pushButton_2_clicked()
{
    exit(0);
}
