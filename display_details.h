#ifndef DISPLAY_DETAILS_H
#define DISPLAY_DETAILS_H


#include <QMainWindow>
#include <stdlib.h>
#include <QString>
#include <QDebug>

class access_page;

namespace Ui {
class display_details;
}

class display_details : public QMainWindow
{
    Q_OBJECT

public:
    explicit display_details(QWidget *parent = 0);
    ~display_details();
    void windowShow(access_page *accessClass);
    void fillAccessDetails(access_page *accessClass);
    QString name;
    QString email;
    QString phone;

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::display_details *ui;
    access_page *access_obj;
};

#endif // DISPLAY_DETAILS_H
