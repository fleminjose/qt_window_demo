#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "access_page.h"
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    access_page access;
};

#endif // MAINWINDOW_H
