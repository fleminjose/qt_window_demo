#ifndef ACCESS_PAGE_H
#define ACCESS_PAGE_H

#include <QMainWindow>
#include "display_details.h"
#include <stdlib.h>
#include <QString>
#include <QDebug>


namespace Ui {
class access_page;
}

class access_page : public QMainWindow
{
    Q_OBJECT

public:
    explicit access_page(QWidget *parent = 0);
    ~access_page();
    QString name;
    QString email;
    QString phone;

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::access_page *ui;
    display_details disp_details;

};

#endif // ACCESS_PAGE_H
