# qt_window_demo

Tested on: 
Qt Creator 4.0.2
Based on Qt 5.7.0
GCC 4.9.1, 64bit

Description:
This application has three windows. First window asks for a password (1234) and redirects to a form if password is correct. Third window confirms the entered detail and exits on clicking 'Finish' button.
